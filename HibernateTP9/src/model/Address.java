package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Address {

	/*
	 * Attributes
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String street1;
	private String street2;
	private String city;
	private String state;
	private String zip;

	@ManyToOne
	private Person person;

	@ManyToOne
	private Company company;

	/*
	 * Builder
	 */

	public Address(String street1, String street2, String city, String state, String zip) {
		super();
		this.street1 = street1;
		this.street2 = street2;
		this.city = city;
		this.state = state;
		this.zip = zip;
	}
	
	/*
	 * Method
	 */
	
	public void addToCompany(Company c) {
		c.addAddress(this);
	}
	
	public void addToPerson(Person p) {
		p.addAddress(this);
	}

	/*
	 * Get & set
	 */

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
