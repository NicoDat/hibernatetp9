package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Company {

	/*
	 * Attributes
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "company")
	private List<Address> addresses;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "company")
	private List<Job> jobs;

	/*
	 * Builder
	 */

	public Company(String name) {
		super();
		this.name = name;
	}
	
	/*
	 * Method
	 */
	
	public void addAddress(Address a) {
		addresses.add(a);
		a.setCompany(this);
	}

	/*
	 * Get & set
	 */

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Job> getJobs() {
		return jobs;
	}

	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	

}
