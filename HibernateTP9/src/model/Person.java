package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Person {

	/*
	 * Attributes
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String firstname;
	private String lastname;
	private char middleInitial;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "person")
	private List<Address> addresses;

	@OneToOne
	private Job job;

	/*
	 * Builder
	 */

	public Person(String firstname, String lastname, char middleInitial) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.middleInitial = middleInitial;
	}
	
	/*
	 * Method
	 */
	
	public void addAddress(Address a) {
		addresses.add(a);
		a.setPerson(this);
	}

	/*
	 * Get & set
	 */

	public long getId() {
		return id;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public char getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(char middleInitial) {
		this.middleInitial = middleInitial;
	}

}
