import controler.HibernateControler;
import controler.InitDatabase;

public class MainApp {

	public static void main(String[] args) {
		HibernateControler.init();

		// start stuff-------------------------------

		// initialize data
		InitDatabase.initData();

		// call controller

		// end stuff---------------------------------

		// close
		HibernateControler.close();

	}

}
