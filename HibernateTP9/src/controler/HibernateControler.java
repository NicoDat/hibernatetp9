package controler;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Manage creation and close of entityManager
 * @author nicolas
 *
 */
public class HibernateControler {
	
	/*
	 * Attributes
	 */
	
	private static EntityManager entityManager = null;
	private static EntityManagerFactory entityManagerFactory = null;

	/*
	 * Methods
	 */
	
	/**
	 * Get EntityManager
	 * @return
	 */
	public static EntityManager getEntityManager() {
		init();
		return entityManager;
	}

	/**
	 * create entityManager if null
	 */
	public static void init() {
		if (entityManager == null) {
			if (entityManagerFactory == null) {
				entityManagerFactory = Persistence.createEntityManagerFactory("master");
			}
			entityManager = entityManagerFactory.createEntityManager();
		}
	}

	/**
	 * close entityManager
	 */
	public static void close() {
		entityManager.close();
		entityManagerFactory.close();
		entityManager = null;
	}
}
