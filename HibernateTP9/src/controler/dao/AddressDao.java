package controler.dao;

import java.util.List;

import javax.persistence.EntityManager;

import controler.HibernateControler;
import model.Address;

public class AddressDao {
	
	/*
	 * Attributes
	 */

	EntityManager em;

	/*
	 * Builder
	 */

	public AddressDao() {
		em = HibernateControler.getEntityManager();
	}

	/*
	 * Methods
	 */

	/**
	 * Add a professor to the DB
	 * 
	 * @param p
	 */
	public void addProfessor(Address a) {
		em.getTransaction().begin();
		em.persist(a);
		em.getTransaction().commit();
	}
	
	/**
	 * return all person in DB
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Address> getAddresses() {
		return em.createQuery("FROM Address").getResultList();
	}
}
