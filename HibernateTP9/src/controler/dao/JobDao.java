package controler.dao;

import java.util.List;

import javax.persistence.EntityManager;

import controler.HibernateControler;
import model.Job;

public class JobDao {

	/*
	 * Attributes
	 */

	EntityManager em;

	/*
	 * Builder
	 */

	public JobDao() {
		em = HibernateControler.getEntityManager();
	}

	/*
	 * Methods
	 */

	/**
	 * Add a professor to the DB
	 * 
	 * @param p
	 */
	public void addCompany(Job j) {
		em.getTransaction().begin();
		em.persist(j);
		em.getTransaction().commit();
	}
	
	/**
	 * return all person in DB
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Job> getJobs() {
		return em.createQuery("FROM Job").getResultList();
	}
	
}
