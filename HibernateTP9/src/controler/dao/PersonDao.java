package controler.dao;

import java.util.List;

import javax.persistence.EntityManager;

import controler.HibernateControler;
import model.Person;

public class PersonDao {
	
	/*
	 * Attributes
	 */

	EntityManager em;

	/*
	 * Builder
	 */

	public PersonDao() {
		em = HibernateControler.getEntityManager();
	}

	/*
	 * Methods
	 */

	/**
	 * Add a professor to the DB
	 * 
	 * @param p
	 */
	public void addProfessor(Person p) {
		em.getTransaction().begin();
		em.persist(p);
		em.getTransaction().commit();
	}
	
	/**
	 * return all person in DB
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Person> getPersons() {
		return em.createQuery("FROM Person").getResultList();
	}
}
