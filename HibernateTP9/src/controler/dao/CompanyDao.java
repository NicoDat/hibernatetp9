package controler.dao;

import java.util.List;

import javax.persistence.EntityManager;

import controler.HibernateControler;
import model.Company;

public class CompanyDao {
	
	/*
	 * Attributes
	 */

	EntityManager em;

	/*
	 * Builder
	 */

	public CompanyDao() {
		em = HibernateControler.getEntityManager();
	}

	/*
	 * Methods
	 */

	/**
	 * Add a professor to the DB
	 * 
	 * @param p
	 */
	public void addCompany(Company c) {
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();
	}
	
	/**
	 * return all person in DB
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Company> getCompanies() {
		return em.createQuery("FROM Company").getResultList();
	}
}
